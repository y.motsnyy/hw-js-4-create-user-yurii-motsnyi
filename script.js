const createNewUser = function () {
  const firstName = prompt('Enter the first name of the new user');
  const lastName = prompt('Enter the last name of the new user');
  
  const newUser = {
    firstName,
    lastName,
    
    getLogin: function () {
      const login = (this.firstName.charAt(0) + this.lastName).toLowerCase();
      return login;
    }
  }
    return newUser;
}
const newUser1 = createNewUser();
const login1 = newUser1.getLogin();
console.log(login1);

const newUser2 = createNewUser();
const login2 = newUser2.getLogin();
console.log(login2);